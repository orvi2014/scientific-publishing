package com.scipub.model;

public enum PaperType {
    RESEARCH, REPORT, SURVEY, POSITION, GENERAL
}
