package com.scipub.model;

public enum PublicationStatus {
    DRAFT, PUBLISHED, RETRACTED, NOT_APPROVED
}
