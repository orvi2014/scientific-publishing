package com.scipub.model;

public enum ResourceType {
    DATASET, FIGURE, CODE, PRESENTATION, MEDIA
}
