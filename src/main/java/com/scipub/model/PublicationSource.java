package com.scipub.model;

public enum PublicationSource {
    SUBMITTED, ARXIV
}
